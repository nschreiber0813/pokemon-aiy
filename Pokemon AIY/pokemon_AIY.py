import os
import tempfile
import time
import traceback
import argparse
import threading

from aiy.board import Board
from aiy.voice.audio import AudioFormat, play_wav, record_file, Recorder

AIY_CARDS = {
    'sndrpigooglevoi': 'Voice HAT (v1)',
    'aiyvoicebonnet': 'Voice Bonnet (v2)'
}

Professor_Oak_Intro_Sound = 'Audio/Professor_Oak_Intro.wav'
with Board() as board:
    done = threading.Event()
    board.button.when_pressed = done.set
    
    def Intro():
        play_wav(Professor_Oak_Intro_Sound)
    def wait():
        
        start = time.monotonic()
        
        while not done.is_set():
            duration = time.monotonic() - start
            
    
    def slot1():
        play_wav('Audio/name.wav')
        recording = False
        slot1_parser = argparse.ArgumentParser()
        slot1_parser.add_argument('--filename', '-f', default='slot1.wav')
        slot1_args = slot1_parser.parse_args()
        record_file(AudioFormat.CD, filename=slot1_args.filename, wait=wait, filetype='wav')
        time.sleep(1.0)
        play_wav(slot1_args.filename)
    Intro()
    slot1()
